using Cinemachine;
using UnityEngine;

public class CameraController : Singleton<CameraController>
{
    private CinemachineVirtualCamera _mainVCam;

    public CinemachineVirtualCamera MainVCam
    {
        get
        {
            if (_mainVCam != null)
                return _mainVCam;
            
            _mainVCam = GetComponent<CinemachineVirtualCamera>();
            
            if (_mainVCam != null)
                return _mainVCam;
            
            return (new GameObject()).AddComponent<CinemachineVirtualCamera>();
        }
    }

    private float _shakeTimer;
    private float _shakeDuration;
    private float _startingIntensity;
    private CinemachineBasicMultiChannelPerlin _perlin;

    public override void Awake()
    {
        base.Awake();
        _perlin = MainVCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    public void ShakeCamera(float intensity, float time)
    {
        _perlin.m_AmplitudeGain = _startingIntensity = intensity;
        _shakeTimer = _shakeDuration = time;
        if (time <= 0)
            _shakeTimer = _shakeDuration = 0.01f; // default time if we put in something dumb
    }

    private void Update()
    {
        UpdateShakeTimer();
    }

    private void UpdateShakeTimer()
    {

        if (_shakeTimer > 0f)
        {
            _perlin.m_AmplitudeGain = Mathf.Lerp(_startingIntensity, 0f, (1f - (_shakeTimer / _shakeDuration)));
            _shakeTimer -= Time.deltaTime;
        }
    }
}
