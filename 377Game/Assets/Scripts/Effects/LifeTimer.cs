﻿using System;
using UnityEngine;

namespace Effects
{
    public class LifeTimer : MonoBehaviour
    {
        [Tooltip("How long until this object is removed.")]
        public float duration = 1f;

        private float _timer;

        private void Update()
        {
            _timer += Time.deltaTime;

            if (_timer >= duration)
            {
                Destroy(gameObject);
            }
        }
    }
}