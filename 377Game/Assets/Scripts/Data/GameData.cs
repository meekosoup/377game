﻿using UI;
using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "GameData", menuName = "Game/GameData", order = 50)]
    public class GameData : ScriptableObject
    {
        public float gameSpeed = 1f;
        public TowerTemplate towerTemplate;
        public GameObject targetUnderFinger;
        [Tooltip("Effect spawned when player releases finger with no valid targets underneath it.")]
        public GameObject cancelDraggingEffect;
        public Vector3 lastDragPoint;
        public bool purchasing;
        public bool dragging;
        public Selection selected;
        public Tower selectedTower;
        public TowerWeapon selectedWeapon;
        public bool paused;

        [Header("Verbiage")] 
        public string dollarSymbol = "$";

        public delegate void DragEvent();

        public DragEvent onPurchaseDragBegin;
        public DragEvent onPurchaseDragEnd;
        public DragEvent onPurchaseCancel;
        
        public delegate void SelectionEvent();

        public SelectionEvent onDeselect;

        public delegate void UnitEvent();

        public UnitEvent onUnitLeavesBattlefield;

        public void Reset()
        {
            gameSpeed = 1f;
            targetUnderFinger = null;
            lastDragPoint = Vector3.zero;
            purchasing = false;
            dragging = false;
        }
    }
}