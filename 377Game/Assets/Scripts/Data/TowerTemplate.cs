﻿using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "TowerTemplate", menuName = "Game/TowerTemplate", order = 52)]
    public class TowerTemplate : ScriptableObject
    {
        [Tooltip("The tower that is actually placed on the battlefield.")]
        public GameObject towerPrefab;
        [Tooltip("The graphic seen while dragging.")]
        public GameObject draggingPrefab;

        public string towerName = "Unknown";
        public int goldCost = 10;
    }
}