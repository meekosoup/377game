using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Game/PlayerData", order = 51)]
public class PlayerData : ScriptableObject
{
    public int health = 100;
    public int healthMax = 100;
    public int gold = 0;
    
    public delegate void HealthEvent();
    public HealthEvent healthEventListener;

    public delegate void GoldEvent();
    public GoldEvent goldEventListener;

    public void ResetHealth() => health = healthMax;

    public void Reset()
    {
        health = 100;
        healthMax = 100;
        gold = 0;
    }
}
