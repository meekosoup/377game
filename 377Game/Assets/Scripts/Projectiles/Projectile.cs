using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{
    public enum ProjectileBehavior
    {
        PhysicsBased,
        Homing,
    }
    
    public Transform target;
    public Transform origin;
    public float duration = 3f;
    public ProjectileBehavior behavior = ProjectileBehavior.PhysicsBased;

    [Header("Damage Properties")]
    public int damage = 5;

    private float _startTime;
    private float _progress;
    private float _lifeTimer;
    private Vector3 _targetPosition;
    private Vector3 _originPosition;
    private Rigidbody _rb;
    private Health _targetHealth;

    public void ResetTime()
    {
        _startTime = Time.time;
        _lifeTimer = duration;

        _rb = GetComponent<Rigidbody>();
        _targetHealth = target.GetComponent<Health>();
        
        if (behavior == ProjectileBehavior.PhysicsBased)
            ArrowArcBehavior();
    }

    private void Update()
    {
        if (behavior == ProjectileBehavior.Homing)
            HomingBehavior();
        
        LifeTimer();
    }

    private void LifeTimer()
    {
        _lifeTimer -= Time.deltaTime;

        if (_lifeTimer <= 0)
        {
            if (_targetHealth != null) 
                _targetHealth.TakeDamage(damage);
            
            Destroy(gameObject);
        }
    }

    private void HomingBehavior()
    {
        _targetPosition = target.position;
        _originPosition = origin.position;

        _progress = Mathf.InverseLerp(_startTime, _startTime + duration, Time.time);
        transform.position = Vector3.Lerp(origin.position, _targetPosition, _progress);

        transform.LookAt(target, Vector3.up);
    }

    private void ArrowArcBehavior()
    {
        _rb.velocity = CalculateVelocity(origin.position, target.position, duration);
        
        transform.LookAt(target, Vector3.up);
    }

    private Vector3 CalculateVelocity(Vector3 origin, Vector3 target, float time)
    {
        // define distance x and y first
        Vector3 distance = target - origin;
        Vector3 distanceXZ = distance;
        distanceXZ.y = 0f;

        // convert to float representing distance
        float Sy = distance.y;
        float Sxz = distanceXZ.magnitude;

        float Vxz = Sxz / time;
        float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distanceXZ.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;
    }
}
