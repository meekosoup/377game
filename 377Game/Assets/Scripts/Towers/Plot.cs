using DefaultNamespace;
using UnityEngine;
using UnityEngine.InputSystem;

public class Plot : MonoBehaviour
{
    public PlayerData playerData;
    public GameData gameData;
    public float cameraShakeIntensity = 2f;
    public float cameraShakeTime = 0.2f;

    private PlayerInputActions _inputActions;
    private bool _built;

    private void Awake()
    {
        _inputActions = new PlayerInputActions();
        _inputActions.Enable();
    }

    private void OnEnable()
    {
        _inputActions.Player.Touch.canceled += FingerUp;
        _inputActions.Enable();
    }

    private void OnDisable()
    {
        _inputActions.Player.Touch.canceled -= FingerUp;
        _inputActions.Disable();
    }

    private void FingerUp(InputAction.CallbackContext context)
    {
        if (_built || !gameData.purchasing)
            return;
        
        // if (Camera.main is { }) _ray = Camera.main.ScreenPointToRay(_touchedPoint);
        if (!gameData.targetUnderFinger)
        {
            Debug.Log($"No target under finger yet!");
            return;
        }

        if (gameObject.name == gameData.targetUnderFinger.name)
        {
            if (playerData.gold < gameData.towerTemplate.goldCost)
            {
                gameData.onPurchaseCancel?.Invoke();
                return;
            }
        
            playerData.gold -= gameData.towerTemplate.goldCost;
            playerData.goldEventListener?.Invoke();
            
            // hide platform
            GetComponent<MeshRenderer>().enabled = false;
            
            // build tower
            Instantiate(gameData.towerTemplate.towerPrefab, transform.position, transform.rotation);
            
            _built = true;
            gameData.purchasing = false;
            
            // shake camera
            CameraController.Instance.ShakeCamera(cameraShakeIntensity, cameraShakeTime);
        
            gameObject.SetActive(false);
        }
    }
}
