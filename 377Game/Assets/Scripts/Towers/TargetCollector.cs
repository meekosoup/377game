using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(SphereCollider)), RequireComponent(typeof(Rigidbody))]
public class TargetCollector : MonoBehaviour
{
    
    public delegate void TargetsChanged();
    public TargetsChanged TargetsChangedListener;
    
    [SerializeField] private List<GameObject> _targets = new List<GameObject>();

    public List<GameObject> Targets
    {
        get => _targets;
        private set => _targets = value;
    }
    
    private SphereCollider _sphereCollider;

    private void Awake()
    {
        _sphereCollider = GetComponent<SphereCollider>();
        _sphereCollider.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log($"{other.gameObject.name} entered!");

        AcquireValidTarget(other);
    }

    private void AcquireValidTarget(Collider other)
    {
        if (other.GetComponent<IEnemy>() != null)
        {
            Health health = other.GetComponent<Health>();
            if (health != null && !health.IsDead())
            {
                _targets.Add(other.gameObject);
                TargetsChangedListener?.Invoke();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Debug.Log($"{other.gameObject.name} exited!");
        
        if (other.GetComponent<IEnemy>() != null)
        {
            _targets.Remove(other.gameObject);
            TargetsChangedListener?.Invoke();
        }
    }

    public void CleanUpTargets()
    {
        // _targets = _targets.Where(item => item != null).ToList();
        for (int i = _targets.Count - 1; i > -1; i--)
        {
            if (_targets[i] == null)
                _targets.RemoveAt(i);
        }
    }
}
