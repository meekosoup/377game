using System;
using System.Collections.Generic;
using UnityEngine;

public class TowerWeapon : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform firePoint;
    public float projectileSpeed = 0.5f;
    public float fireRate = 0.5f;

    [SerializeField]
    private TargetCollector _targetCollector;
    private List<GameObject> _targets = new List<GameObject>();
    private float _fireTimer;

    private void Awake()
    {
        _targetCollector = GetComponentInChildren<TargetCollector>();
        _targets = _targetCollector.Targets;
    }

    private void Update()
    {
        if (_fireTimer > 0)
            _fireTimer -= Time.deltaTime;

        if (_fireTimer <= 0 && _targets.Count > 0)
        {
            Fire();
            _fireTimer = fireRate;
        }
    }

    private void Fire()
    {
        _targetCollector.CleanUpTargets();
        
        if (_targets.Count <= 0)
            return;
        
        // Debug.Log($"Firing at {_targets[0].name}");

        GameObject projectileObject = Instantiate(projectilePrefab, firePoint.transform.position, projectilePrefab.transform.rotation);
        Projectile projectile = projectileObject.GetComponent<Projectile>();
        
        if (projectile)
        {
            projectile.origin = firePoint;
            projectile.target = _targets[0].transform;
            projectile.duration = projectileSpeed;
            projectile.ResetTime();
        }
    }
}
