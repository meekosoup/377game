﻿using System;
using UnityEngine;

namespace GameControl
{
    public class DoNotDestroy : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}