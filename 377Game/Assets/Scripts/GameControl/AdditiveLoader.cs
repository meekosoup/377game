﻿using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameControl
{
    public class AdditiveLoader : MonoBehaviour
    {
        public string sceneName;
        [ReadOnly]
        private Dictionary<string, bool> loadedScenes = new Dictionary<string, bool>();

        private void OnEnable()
        {
            SceneManager.sceneLoaded += SceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= SceneLoaded;
        }

        private void SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (loadedScenes.ContainsKey(scene.name))
                loadedScenes.Add(scene.name, scene.isLoaded);
            else
                loadedScenes[scene.name] = scene.isLoaded;
        }

        private void Start()
        {
            if (!loadedScenes.ContainsKey(sceneName))
                SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }
    }
}