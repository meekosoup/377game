﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameControl
{
    public class GameManager : MonoBehaviour
    {
        public GameData gameData;
        public PlayerData playerData;
        public int startingValue;
        public string nextScene = "Level1";
        private List<WaveSpawner> _waveSpawners = new List<WaveSpawner>();
        [SerializeField, ReadOnly]
        private int _totalUnits;

        private void Awake()
        {
            gameData.Reset();
            playerData.Reset();
            
            playerData.gold = startingValue;
        }

        private void Start()
        {
            _waveSpawners = FindObjectsOfType<WaveSpawner>().ToList();
            _totalUnits = 0;
            foreach (WaveSpawner waveSpawner in _waveSpawners)
            {
                _totalUnits += waveSpawner.GetMaxPotentialUnits();
            }
        }

        private void OnEnable()
        {
            gameData.onUnitLeavesBattlefield += UnitLeftBattlefield;
        }

        private void OnDisable()
        {
            gameData.onUnitLeavesBattlefield -= UnitLeftBattlefield;
        }

        private void UnitLeftBattlefield()
        {
            _totalUnits--;
            
            if (_totalUnits <= 0)
                NextScene();
        }

        private void NextScene()
        {
            SceneUtility.LoadLevel(nextScene);
        }
    }
}