using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int Current
    {
        get => _amount;
        set
        {
            _amount = (int)Mathf.Clamp(value, 0, _maxHealth);
            HealthChangedListener?.Invoke();
        }
    }

    public int MaxHealth
    {
        get => _maxHealth;
        set => _maxHealth = value;
    }

    public delegate void HealthChanged();
    public HealthChanged HealthChangedListener;

    [SerializeField]
    private int _amount = 100;
    [SerializeField]
    private int _maxHealth = 100;

    public void TakeDamage(int amount)
    {
        Current -= amount;
    }

    public bool IsDead()
    {
        return Current <= 0;
    }
}
