using System;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Events;

public class UnitExitZone : MonoBehaviour
{
    public GameData gameData;
    public PlayerData playerData;
    public UnityEvent gameOverAction;

    private void OnEnable()
    {
        playerData.healthEventListener += GameOverAction;
    }

    private void OnDisable()
    {
        playerData.healthEventListener -= GameOverAction;
    }

    private void OnTriggerEnter(Collider other)
    {
        ContactDamage contactDamage = other.GetComponent<ContactDamage>();

        if (!contactDamage)
            contactDamage = other.GetComponentInChildren<ContactDamage>();
        
        if (!contactDamage)
            return;

        playerData.health -= contactDamage.damage;
        playerData.healthEventListener?.Invoke();
        gameData.onUnitLeavesBattlefield?.Invoke();
        
        // Replace this with some kind of pooling system
        Destroy(other.gameObject);
    }

    private void GameOverAction()
    {
        if (playerData.health <= 0)
        {
            gameOverAction?.Invoke();
        }
    }
}
