using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{

    [TableList]
    public List<Wave> waves = new List<Wave>();

    private int _waveIndex;
    private float _waveTimer;
    private int _leftToSpawn;
    private float _spawnTimer;

    private void Awake()
    {
        if (waves.Count > 0)
            _waveTimer = waves[_waveIndex].time;
    }

    private void Update()
    {
        WaveTimerEvent();
    }

    private void OnValidate()
    {
        Transform lastStart = null;
        Transform lastEnd = null;
        GameObject lastUnit = null;
        
        foreach (Wave wave in waves)
        {
            if (wave.start)
                lastStart = wave.start;
            else
            {
                if (!wave.start)
                    wave.start = lastStart;
            }
            
            if (wave.end)
                lastEnd = wave.end;
            else
            {
                if (!wave.end)
                    wave.end = lastEnd;
            }
            
            if (wave.unitPrefab)
                lastUnit = wave.unitPrefab;
            else
            {
                if (!wave.unitPrefab)
                    wave.unitPrefab = lastUnit;
            }
        }
    }

    private void WaveTimerEvent()
    {
        if (_waveIndex >= waves.Count)
            return;

        if (_waveTimer > 0)
        {
            _waveTimer -= Time.deltaTime;
            return;
        }

        if (_leftToSpawn <= 0)
            _leftToSpawn = waves[_waveIndex].amount;

        if (_leftToSpawn > 0 && _spawnTimer > 0)
            _spawnTimer -= Time.deltaTime;
        
        if (_spawnTimer <= 0)
        {
            SpawnUnit();
            _leftToSpawn--;
            _spawnTimer = waves[_waveIndex].spawnRate;
        }
        
        if (_leftToSpawn <= 0)
        {
            _waveIndex++;
            
            if (_waveIndex >= waves.Count)
                return;
            
            _waveTimer = waves[_waveIndex].time;
        }
    }

    private void SpawnUnit()
    {
        Transform start, end;
        start = waves[_waveIndex].start;
        end = waves[_waveIndex].end;

        if (start == null)
            start = transform;

        if (end == null)
            end = transform;
        
        GameObject unit = Instantiate(waves[_waveIndex].unitPrefab, start.position, Quaternion.identity);

        UnitController unitController = unit.GetComponent<UnitController>();
        if (unitController != null)
        {
            unitController.destination = end;
            unitController.StartMove();
        }
    }

    public int GetMaxPotentialUnits()
    {
        int amount = 0;
        
        foreach (Wave wave in waves)
        {
            amount += wave.amount;
        }

        return amount;
    }

}
