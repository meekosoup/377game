using System;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class UnitController : MonoBehaviour, IEnemy
{
    public GameData gameData;
    public PlayerData playerData;
    public Transform destination;
    public LayerMask layerMask;

    private PlayerInputActions _playerInputAction;
    private float heighOffset = 2f;
    private NavMeshAgent _navAgent;
    private Vector2 _touchPoint;
    private RaycastHit _hit;
    private Ray _ray;
    private Health _health;

    private void Awake()
    {
        _playerInputAction = new PlayerInputActions();
        _playerInputAction.Enable();
        
        _navAgent = GetComponent<NavMeshAgent>();

        _health = GetComponent<Health>();
    }

    private void Start()
    {
        destination = GameObject.Find("EndMarker").transform;
    }

    private void OnEnable()
    {
        // _playerInputAction.Player.Touch.performed += UpdateTouchPoint;
        // _playerInputAction.Player.Touch.performed += UpdateDestination;

        if (_health)
            _health.HealthChangedListener += DefeatUnit;
    }

    private void OnDisable()
    {
        // _playerInputAction.Player.Touch.performed -= UpdateTouchPoint;
        // _playerInputAction.Player.Touch.performed -= UpdateDestination;

        if (_health)
            _health.HealthChangedListener -= DefeatUnit;
    }

    private void UpdateTouchPoint(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            _touchPoint = _playerInputAction.Player.TouchPoint.ReadValue<Vector2>();
        }
    }

    public void StartMove()
    {
        _navAgent.SetDestination(destination.position);
    }

    private void UpdateDestination(InputAction.CallbackContext ctx)
    {
        if (ctx.performed && destination != null)
        {
            if (Camera.main is { }) _ray = Camera.main.ScreenPointToRay(_touchPoint);

            if (Physics.Raycast(_ray, out _hit, Mathf.Infinity, layerMask))
            {
                destination.position = _hit.point + Vector3.up * heighOffset;
                _navAgent.SetDestination(destination.position);
            }
        }
    }

    private void RemoveUnit()
    {
        gameData.onUnitLeavesBattlefield?.Invoke();
        Destroy(gameObject);
    }

    private void DefeatUnit()
    {
        if (!(_health && _health.IsDead()))
            return;

        Gold gold = GetComponent<Gold>();
        playerData.gold += gold.gold;
        playerData.goldEventListener?.Invoke();
        gameData.onUnitLeavesBattlefield?.Invoke();
        Destroy(gameObject);
    }
}
