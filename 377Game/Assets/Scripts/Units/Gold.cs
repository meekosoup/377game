using Sirenix.OdinInspector;
using UnityEngine;

public class Gold : MonoBehaviour
{
    [InfoBox("Amount of gold gained when this unit dies.")]
    public int gold = 100;
}
