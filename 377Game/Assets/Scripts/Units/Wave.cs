using System;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class Wave
{
    [HorizontalGroup("Time | Unit | Amount | Spawn Rate"), HideLabel, Min(0.1f)]
    public float time = 1f;
    [HorizontalGroup("Time | Unit | Amount | Spawn Rate"), HideLabel]
    public GameObject unitPrefab;
    [HorizontalGroup("Time | Unit | Amount | Spawn Rate"), HideLabel, Min(1)]
    public int amount = 1;
    [HorizontalGroup("Time | Unit | Amount | Spawn Rate"), HideLabel, Min(0.1f)]
    public float spawnRate = 0.5f;
    [HorizontalGroup("Start | End"), HideLabel]
    public Transform start;
    [HorizontalGroup("Start | End"), HideLabel]
    public Transform end;
}
