using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        public float halfHealth = 0.5f;
        public float quarterHealth = 0.25f;
        public Color halfHealthColor = Color.yellow;
        public Color quarterHealthColor = Color.red;
        public Image fillSprite;

        private Health _health;
        private Slider _slider;
        private Vector3 _rot;

        private void Awake()
        {
            _health = transform.parent.GetComponent<Health>();
            _slider = GetComponentInChildren<Slider>();
        }

        private void Update()
        {
            if (Camera.main is { })
            {
                transform.LookAt(Camera.main.transform);
                _rot = transform.rotation.eulerAngles;
                transform.rotation = Quaternion.Euler(_rot.x, _rot.y, 0f);
                // transform.rotation = Quaternion.Euler(_rot.x, 0f, _rot.z);
                // transform.rotation = Quaternion.Euler(0f, _rot.y, _rot.z);;
            }

            if (_health != null && _slider != null)
            {
                _slider.minValue = 0;
                _slider.maxValue = _health.MaxHealth;
                _slider.value = _health.Current;

                if (_health.Current < _health.MaxHealth * halfHealth)
                    fillSprite.color = halfHealthColor;

                if (_health.Current < _health.MaxHealth * quarterHealth)
                    fillSprite.color = quarterHealthColor;
            }
            else
            {
                Debug.Log($"health is {_health} and slider is {_slider}");
            }
        }
    }
}
