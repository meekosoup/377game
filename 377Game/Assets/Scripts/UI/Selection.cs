using DefaultNamespace;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UI
{
    public class Selection : MonoBehaviour
    {
        public GameData gameData;
        public GameObject selectionCircle;
        public LayerMask layerMask;

        private PlayerInputActions _inputActions;
        private Vector2 _touchedPoint;
        private RaycastHit _hit;
        private Ray _ray;

        private void Awake()
        {
            _inputActions = new PlayerInputActions();
            _inputActions.Enable();
            
            if (selectionCircle)
                selectionCircle.SetActive(false);
        }

        private void OnEnable()
        {
            _inputActions.Player.TouchPoint.performed += TouchPoint;
            _inputActions.Player.Touch.performed += Touch;
            gameData.onDeselect += Deselect;
            _inputActions.Enable();
        }

        private void OnDisable()
        {
            _inputActions.Player.TouchPoint.performed -= TouchPoint;
            _inputActions.Player.Touch.performed -= Touch;
            gameData.onDeselect -= Deselect;
            _inputActions.Disable();
        }

        private void TouchPoint(InputAction.CallbackContext context)
        {
            _touchedPoint = context.ReadValue<Vector2>();
        }

        private void Touch(InputAction.CallbackContext context)
        {
            // if (Camera.main is { }) _ray = Camera.main.ScreenPointToRay(_touchedPoint);
            //
            // if (Physics.Raycast(_ray, out _hit, Mathf.Infinity, layerMask))
            // {
            //     GameObject parent = _hit.collider.transform.parent.gameObject; 
            //     
            //     if (parent == gameObject) 
            //         Select();
            //     else
            //         Deselect();
            // }
            // else
            // {
            //     Deselect();
            // }
        }

        public void Select()
        {
            selectionCircle.SetActive(true);
        }

        public void Deselect()
        {
            selectionCircle.SetActive(false);
        }

        private void ToggleSelection()
        {
            selectionCircle.SetActive(!selectionCircle.activeSelf);
        }
    }
}
