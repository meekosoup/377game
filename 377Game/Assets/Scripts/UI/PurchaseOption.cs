﻿using DefaultNamespace;
using TMPro;
using UnityEngine;

namespace UI
{
    public class PurchaseOption : MonoBehaviour
    {
        public GameData gameData;
        public TMP_Text towerNameText;
        public TMP_Text towerCostText;
        public TowerTemplate towerTemplate;

        public void UpdateText()
        {
            towerNameText.text = $"{towerTemplate.towerName}";
            towerCostText.text = $"{gameData.dollarSymbol}{towerTemplate.goldCost.ToString()}";
        }

        public void DragPurchase()
        {
            // Debug.Log("Clicked!");
            gameData.towerTemplate = towerTemplate;
            gameData.purchasing = true;
            gameData.onPurchaseDragBegin?.Invoke();
        }
    }
}