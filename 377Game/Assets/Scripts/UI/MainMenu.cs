using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenu : MonoBehaviour
    {
        public void ExitButton()
        {
            Application.Quit();
            Debug.Log("Exit Button Executed");
        }

        public void StartGame()
        {
            SceneManager.LoadScene(1);
        }
    }
}
