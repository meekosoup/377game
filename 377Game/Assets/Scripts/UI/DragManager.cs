using DefaultNamespace;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;

namespace UI
{
    [DefaultExecutionOrder(-1)]
    public class DragManager : MonoBehaviour
    {
        public GameData gameData;
        public GameObject dragPrefab;
        public LayerMask terrainLayer;
        public LayerMask towerLayer;
        public Vector2 _touchPoint;
        
        private PlayerInputActions _inputActions;
        private Ray _ray;
        private RaycastHit _hit;
        private GameObject _dragInstance;

        private void Awake()
        {
            _inputActions = new PlayerInputActions();
            
            if (gameData.towerTemplate)
                InitializeDraggablePrefab();
        }

        private void OnEnable()
        {
            _inputActions.Player.TouchPoint.performed += TouchMove;
            gameData.onPurchaseDragBegin += TouchDown;
            gameData.onPurchaseCancel += CancelEffect;
            _inputActions.Player.Touch.canceled += TouchUp;
            
            if (_dragInstance)
                _dragInstance.SetActive(true);
            
            _inputActions.Enable();
            TouchSimulation.Enable();
        }

        private void OnDisable()
        {
            _inputActions.Player.TouchPoint.performed -= TouchMove;
            gameData.onPurchaseDragBegin -= TouchDown;
            gameData.onPurchaseCancel -= CancelEffect;
            _inputActions.Player.Touch.canceled -= TouchUp;
            
            if (_dragInstance)
                _dragInstance.SetActive(false);
            
            // _inputActions.Disable();
            TouchSimulation.Disable();
        }

        private void InitializeDraggablePrefab()
        {
            // TODO: very inefficient
            if (_dragInstance)
                Destroy(_dragInstance.gameObject);
            
            _dragInstance = Instantiate(gameData.towerTemplate.draggingPrefab, transform.position, Quaternion.identity);
        }

        private void TouchDown()
        {
            if (!gameData.purchasing)
                return;
            
            InitializeDraggablePrefab();
            _dragInstance.SetActive(true);
            gameData.dragging = true;
        }

        private void TouchDown(InputAction.CallbackContext context)
        {
            PurchaseDragOn();
        }

        private void PurchaseDragOn()
        {
            if (!gameData.purchasing)
                return;
            
            InitializeDraggablePrefab();
            _dragInstance.SetActive(true);
            gameData.dragging = true;
        }

        private void TouchUp(InputAction.CallbackContext context)
        {
            SelectionCheck();
            PurchaseDragOff();
        }

        private void SelectionCheck()
        {
            if (Camera.main != null) _ray = Camera.main.ScreenPointToRay(_touchPoint);

            if (Physics.Raycast(_ray, out _hit, Mathf.Infinity, towerLayer))
            {
                GameObject parent = _hit.collider.transform.parent.gameObject;
                gameData.selected = parent.GetComponent<Selection>();
                gameData.selectedTower = parent.GetComponent<Tower>();
                gameData.selectedWeapon = parent.GetComponent<TowerWeapon>();

                if (gameData.selected)
                {
                    gameData.onDeselect?.Invoke();
                    gameData.selected.Select();
                }
            }
            else
            {
                gameData.selected = null;
                gameData.selectedTower = null;
                gameData.selectedWeapon = null;
                gameData.onDeselect?.Invoke();
            }
        }

        private void PurchaseDragOff()
        {
            if (gameData.targetUnderFinger == null)
                gameData.onPurchaseCancel?.Invoke();
            
            gameData.onPurchaseDragEnd?.Invoke();
            _dragInstance.SetActive(false);
            gameData.dragging = false;
        }

        private void CancelEffect()
        {
            if (gameData.purchasing && gameData.cancelDraggingEffect)
            {
                Instantiate(gameData.cancelDraggingEffect, gameData.lastDragPoint, Quaternion.identity);
                gameData.purchasing = false;
            }
        }

        private void TouchMove(InputAction.CallbackContext context)
        {
            _touchPoint = context.ReadValue<Vector2>();
            if (!gameData.dragging && !gameData.purchasing)
                return;
            
            _ray = Camera.main.ScreenPointToRay(_touchPoint);
            
            if (Physics.Raycast(_ray, out _hit, Mathf.Infinity, terrainLayer))
            {
                _dragInstance.transform.position = _hit.point;
                gameData.lastDragPoint = _hit.point;
            }
            
            if (Physics.Raycast(_ray, out _hit, Mathf.Infinity, towerLayer))
            {
                _dragInstance.transform.position = _hit.collider.transform.position;
                gameData.targetUnderFinger = _hit.collider.gameObject;
            }
            else
            {
                gameData.targetUnderFinger = null;
            }
        }
    }
}
