using DefaultNamespace;
using TMPro;
using UnityEngine;

namespace UI
{
    public class GameSpeedController : MonoBehaviour
    {
        public GameData gameData;
        public float gameSpeedIncrement = 1f;
        public float gameSpeedMax = 3f;
        public TMP_Text gameSpeedText;
        public TMP_Text gamePauseText;
        private float _lastGameSpeed;

        private void Start()
        {
            gameData.gameSpeed = 1f;
            gameData.paused = false;
            Time.timeScale = gameData.gameSpeed;
            
            UpdateText();
        }

        public void IncreaseGameSpeed()
        {
            UnpauseGame();
            
            gameData.gameSpeed += gameSpeedIncrement;

            if (gameData.gameSpeed > gameSpeedMax)
                gameData.gameSpeed = 1f;

            Time.timeScale = gameData.gameSpeed;

            UpdateText();
        }

        public void DecreaseGameSpeed()
        {
            UnpauseGame();
            
            gameData.gameSpeed -= gameSpeedIncrement;

            if (gameData.gameSpeed < 1f)
                gameData.gameSpeed = 1f;

            Time.timeScale = gameData.gameSpeed;

            UpdateText();
        }

        public void PauseGame()
        {
            if (gameData.paused)
                return;
            
            _lastGameSpeed = gameData.gameSpeed;
            gameData.gameSpeed = 0f;
            gameData.paused = true;

            Time.timeScale = gameData.gameSpeed;

            UpdateText();
        }

        public void UnpauseGame()
        {
            if (!gameData.paused)
                return;
            
            gameData.gameSpeed = _lastGameSpeed;
            gameData.paused = false;
            
            if (gameData.gameSpeed < 1f)
                gameData.gameSpeed = 1f;
            if (gameData.gameSpeed > gameSpeedMax)
                gameData.gameSpeed = gameSpeedMax;

            Time.timeScale = gameData.gameSpeed;

            UpdateText();
        }

        public void TogglePauseGame()
        {
            if (gameData.paused)
                UnpauseGame();
            else
                PauseGame();
        }

        private void UpdateText()
        {
            gameSpeedText.text = $"{gameData.gameSpeed.ToString()}x";
            gamePauseText.text = (!gameData.paused) ? $"ll" : $">";
        }
    }
}
