using System;
using TMPro;
using UnityEngine;

namespace UI
{
    public class GoldUI : MonoBehaviour
    {
        public PlayerData playerData;
        public TMP_Text goldText;
        public int startingValue = 20;

        private void Start()
        {
            if (!goldText)
                goldText = GameObject.Find("GoldText").GetComponent<TMP_Text>();

            UpdateGold();
        }

        private void OnEnable()
        {
            playerData.goldEventListener += UpdateGold;
        }

        private void OnDisable()
        {
            playerData.goldEventListener -= UpdateGold;
        }

        private void UpdateGold()
        {
            goldText.text = $"{playerData.gold.ToString()}";
        }
    }
}
