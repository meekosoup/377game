using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace UI
{
    public class SignButton : MonoBehaviour
    {
        public UnityEvent unityEvent;
        public LayerMask layerMask;
        private PlayerInputActions _inputActions;
        private Vector2 _touchedPoint;
        private Ray _ray;
        private RaycastHit _hit;

        private void Awake()
        {
            InitializeInputActions();
        }

        private void InitializeInputActions()
        {
            _inputActions = new PlayerInputActions();
            _inputActions.Enable();
        }

        private void OnEnable()
        {
            _inputActions.Player.TouchPoint.performed += TouchPoint;
            _inputActions.Player.Touch.performed += Touch;
        }

        private void OnDisable()
        {
            _inputActions.Player.TouchPoint.performed -= TouchPoint;
            _inputActions.Player.Touch.performed -= Touch;
        }

        private void TouchPoint(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _touchedPoint = context.ReadValue<Vector2>();
            }
        }

        private void Touch(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if (Camera.main is { }) _ray = Camera.main.ScreenPointToRay(_touchedPoint);

                if (Physics.Raycast(_ray, out _hit, Mathf.Infinity, layerMask) &&
                    _hit.collider.gameObject == gameObject)
                {
                    Debug.Log($"Performing button event!");
                    unityEvent.Invoke();
                }
            }
        }
    }
}
