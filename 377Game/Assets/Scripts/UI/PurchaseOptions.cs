﻿using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

namespace UI
{
    public class PurchaseOptions : MonoBehaviour
    {
        [Tooltip("The panel all the options live on. Will probably have a horizontal layout group.")]
        public GameObject purchaseOptionsPanel;
        [Tooltip("The object that lives on the panel (usually has a button attached to it)")]
        public GameObject purchaseOptionPrefab;
        public List<TowerTemplate> towersTemplates = new List<TowerTemplate>();

        private GameObject _prefabInstance;
        private List<GameObject> _purchaseButtons = new List<GameObject>();

        private void Awake()
        {
            foreach (var towerTemplate in towersTemplates)
            {
                _prefabInstance = null;
                _prefabInstance = Instantiate(purchaseOptionPrefab, purchaseOptionsPanel.transform, true);
                PurchaseOption option = _prefabInstance.GetComponent<PurchaseOption>();
                _prefabInstance.transform.localScale = Vector3.one;
                option.towerTemplate = towerTemplate;
                option.UpdateText();
                _purchaseButtons.Add(_prefabInstance);
            }
        }
    }
}