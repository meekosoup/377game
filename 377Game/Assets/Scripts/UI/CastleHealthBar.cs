using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CastleHealthBar : MonoBehaviour
    {
        public PlayerData playerData;
    
        private Slider _slider;

        private void Awake()
        {
            playerData.ResetHealth();
        
            _slider = GetComponentInChildren<Slider>();
            _slider.minValue = 0;
            _slider.maxValue = playerData.healthMax;
            _slider.value = playerData.health;
        }

        private void OnEnable()
        {
            playerData.healthEventListener += UpdateDisplay;
        }

        private void OnDisable()
        {
            playerData.healthEventListener -= UpdateDisplay;
        }

        private void UpdateDisplay()
        {
            _slider.value = playerData.health;
        }
    }
}
