﻿using System;
using DefaultNamespace;
using TMPro;
using UnityEngine;

namespace UI
{
    public class PurchaseControls : MonoBehaviour
    {
        public GameData gameData;
        public GameObject purchaseOptionsPanel;
        public TMP_Text expandButtonText;
        
        private bool _purchasePanelWasOpen;

        private void Awake()
        {
            purchaseOptionsPanel.SetActive(false);
            UpdateExpandText();
        }

        private void OnEnable()
        {
            gameData.onPurchaseDragBegin += TogglePurchaseOptions;
            gameData.onPurchaseDragEnd += ReopenIfPurchasing;
            gameData.onPurchaseCancel += ReopenIfPurchasing;
        }

        private void OnDisable()
        {
            gameData.onPurchaseDragBegin -= TogglePurchaseOptions;
            gameData.onPurchaseDragEnd -= ReopenIfPurchasing;
            gameData.onPurchaseCancel -= ReopenIfPurchasing;
        }

        public void TogglePurchaseOptions()
        {
            Debug.Log($"Toggle Purchase Options");
            _purchasePanelWasOpen = purchaseOptionsPanel.activeSelf;
            purchaseOptionsPanel.SetActive(!purchaseOptionsPanel.activeSelf);
            UpdateExpandText();
        }

        private void ReopenIfPurchasing()
        {
            if (_purchasePanelWasOpen)
            {
                purchaseOptionsPanel.SetActive(true);
                _purchasePanelWasOpen = false;
                UpdateExpandText();
            }
        }

        private void UpdateExpandText()
        {
            expandButtonText.text = purchaseOptionsPanel.activeSelf ? "<" : ">";
        }
    }
}